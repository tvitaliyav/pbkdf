﻿// PBKDF.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "hmac.h"
#include "sha256.h"
#include "pbkdf2.h"
#include <ctime>
#include <psapi.h>
#include <omp.h>
using namespace cppcrypto;
const int g_nNumberOfThreads = 8;

std::string convertToString(char* a, int size)
{
    int i;
    std::string s = "";
    for (i = 0; i < size; i++) {
        s = s + a[i];
    }
    return s;
}

int main()
{

    hmac Hmac(sha256(), "password");
    unsigned char key[16/ 8];
    unsigned char key_[16 / 8];
    int start_time = clock();
   
        pbkdf2(Hmac, (const unsigned char*)"salt", 4, 2000, key, sizeof(key));
        //PROCESS_MEMORY_COUNTERS pmc;
        //GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
        //std::cout << (pmc.WorkingSetSize / 1024) << std::endl; // в КБ
       // time_k[j] = (double)(clock() - start_time) / CLOCKS_PER_SEC;
        std::cout << "PBKDF2 runtime = " << (double)(clock() - start_time) / CLOCKS_PER_SEC << std::endl; // время работы программы 
        std::cout << key << std::endl;
       // hmac Hmac_(sha256(), "AAAAFjQB");
       // pbkdf2(Hmac_, (const unsigned char*)"salt", 4, 1000, key_, sizeof(key_));
       // std::cout << key << std::endl;
        char string[8];
        int first = 65;
        int last = 122;
        bool isNotEquals = true;

        omp_set_num_threads(g_nNumberOfThreads);
#pragma omp parallel for shared(isNotEquals) collapse(8)
        for (int i = first; i < last; i++) {
            for (int j = first; j < last; j++) {
                for (int k = first; k < last; k++) {
                    for (int t = first; t < last; t++) {
                        for (int h = first; h < last; h++) {
                            for (int m = first; m < last; m++) {
                                for (int n = first; n < last; n++) {
                                    for (int p = first; p < last; p++) {
                                        if (isNotEquals) {
                                            string[0] = i;
                                            string[1] = j;
                                            string[2] = k;
                                            string[3] = t;
                                            string[4] = h;
                                            string[5] = m;
                                            string[5] = m;
                                            string[6] = n;
                                            string[7] = p;

                                            hmac hmac_(sha256(), convertToString(string, sizeof(string)));
                                            pbkdf2(hmac_, (const unsigned char*)"salt", 4, 2000, key_, sizeof(key_));
#pragma omp critical
                                            {
                                                bool isEquals = true;
                                                for (int x = 0; x < sizeof(key); x++) {
                                                    if (key[x] != key_[x]) {
                                                        isEquals = false;
                                                        break;
                                                    }
                                                }
                                                std::cout << convertToString(string, sizeof(string)) << std::endl;
                                                if (isEquals) {
                                                    std::cout << "World is " << string << std::endl;
                                                    std::cout << "PBKDF2 runtime for collision = " << (double)(clock() - start_time) / CLOCKS_PER_SEC << std::endl;
                                                    isNotEquals = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
}
