
#include "pbkdf2.h"
#include "portability.h"
#include <algorithm>
#include <memory.h>
#include <psapi.h>
#include <iostream>
#define min(a,b)            (((a) < (b)) ? (a) : (b))

namespace cppcrypto

{
	struct HashNode
	{
		int id;
		unsigned char* hash;
		HashNode* left;          
		HashNode* right;
	};

	void getTmp(unsigned char** temp, unsigned char* temp1, int lenght, int count) {
		int rest = lenght % 10;
		for (int i = 0; i < lenght - rest; i += 10) {
			for (int j = 0; j < count; j++) {
				temp1[j] = ((temp[i][j] ^ temp[i + 1][j]) << 3) ^
					((temp[i + 1][j] ^ temp[i + 8][j]) << 5) ^
					((temp[i + 2][j] ^ temp[i + 5][j]) << 7) ^ 
					((temp[i + 3][j] ^ temp[i + 4][j]) << 11) ^
					((temp[i + 4][j] ^ temp[i + 9][j]) << 13) ^
					((temp[i + 5][j] ^ temp[i][j]) << 17) ^
					((temp[i + 6][j] ^ temp[i + 3][j]) << 19) ^
					((temp[i + 7][j] ^ temp[i + 6][j]) << 23) ^
					((temp[i + 8][j] ^ temp[i + 7][j]) << 27) ^
					((temp[i + 9][j] ^ temp[i + 2][j]) << 29);
			}
		}
	}

	void getTmp2(unsigned char** temp, unsigned char* temp1, int lenght, int count) {
		int rest = lenght % 10;
		for (int i = 0; i < lenght - rest; i += 10) {
			for (int j = 0; j < count; j++) {
				unsigned char t1 = ((temp[i + 1][j] ^ temp[i + 8][j]) << 5) ^
					((temp[i + 3][j] ^ temp[i + 4][j]) << 11) ^
					((temp[i + 5][j] ^ temp[i][j]) << 17) ^
					((temp[i + 7][j] ^ temp[i + 6][j]) << 23) ^
					((temp[i + 9][j] ^ temp[i + 2][j]) << 29);
				unsigned char t2 = ((temp[i][j] ^ temp[i + 1][j]) << 3) ^
					((temp[i + 2][j] ^ temp[i + 5][j]) << 7) ^
					((temp[i + 4][j] ^ temp[i + 9][j]) << 13) ^
					((temp[i + 6][j] ^ temp[i + 3][j]) << 19) ^
					((temp[i + 8][j] ^ temp[i + 7][j]) << 27);
				temp1[count - j - 1] = t1 ^ t2;
			}
		}
	}

	void getTmp3(unsigned char** temp, unsigned char* temp1, int lenght, int count) {
		int rest = lenght % 100;
		for (int i = 0; i < lenght - rest; i += 10) {
			unsigned char t1, t2;
			if (count > 0) {
				getTmp3(temp, temp1, lenght, count - 1);
			}
			t1 = ((temp[i + 1][count] ^ temp[i + 8][count]) << 5) ^
				((temp[i + 3][count] ^ temp[i + 4][count]) << 11) ^
				((temp[i + 5][count] ^ temp[i][count]) << 17) ^
				((temp[i + 7][count] ^ temp[i + 6][count]) << 23) ^
				((temp[i + 9][count] ^ temp[i + 2][count]) << 29);
			t2 = ((temp[i][count] ^ temp[i + 1][count]) << 3) ^
				((temp[i + 2][count] ^ temp[i + 5][count]) << 7) ^
				((temp[i + 4][count] ^ temp[i + 9][count]) << 13) ^
				((temp[i + 6][count] ^ temp[i + 3][count]) << 19) ^
				((temp[i + 8][count] ^ temp[i + 7][count]) << 27);
			temp1[count] = t1 ^ t2;
		
		}
	}

	void hashesInit(unsigned char** hashes, const unsigned char* key, int iterations, int hashLenght, int keyLenght, int seed) {

		srand(seed);
		for (int i = 0; i < iterations - 1; i++) {
			for (int j = 0; j < hashLenght - 1; j++) {
				hashes[i][j] = rand();
			}
		}
		int k = 0;
		for (int i = 0; i < iterations - 1; i++) {
			k = (k + hashes[i][k % hashLenght] + key[i % keyLenght]) % (iterations);
			unsigned char* tmp = new unsigned char[hashLenght];
			memcpy(tmp, hashes[i], hashLenght);
			memcpy(hashes[i], hashes[k], hashLenght);
			memcpy(hashes[k], tmp, hashLenght);
			delete[] tmp;
		}
	}

	void getTmp4(unsigned char** temp, unsigned char* temp1, int lenght, int count) {
		int rest = lenght % 10;
		for (int i = 0; i < lenght - rest; i += 10) {
			for (int j = 0; j < count; j++) {
				temp1[j] = ((temp[i][j] ^ temp[i + 1][j]) << 3) ^
					((temp[i + 1][j] ^ temp[i + 8][j]) << 5) ^
					((temp[i + 2][j] ^ temp[i + 5][j]) << 7) ^
					((temp[i + 3][j] ^ temp[i + 4][j]) << 11) ^
					((temp[i + 4][j] ^ temp[i + 9][j]) << 13) ^
					((temp[i + 5][j] ^ temp[i][j]) << 17) ^
					((temp[i + 6][j] ^ temp[i + 3][j]) << 19) ^
					((temp[i + 7][j] ^ temp[i + 6][j]) << 23) ^
					((temp[i + 8][j] ^ temp[i + 7][j]) << 27) ^
					((temp[i + 9][j] ^ temp[i + 2][j]) << 29);
			}
		}
	}

	void hashesNodeInit(HashNode* hashes_, const unsigned char* key, int iterations, int hashLenght, int keyLenght, int seed) {
		srand(seed);
		for (int i = 0; i < iterations - 1; i++) {
			for (int j = 0; j < hashLenght - 1; j++) {
				hashes_[i].hash[j] = rand();
				hashes_[i].id = i;
			}
		}
		int k = 0;
		for (int i = 0; i < iterations - 1; i++) {
			k = (k + hashes_[i].hash[k % hashLenght] + key[i % keyLenght]) % (iterations);
			unsigned char* tmp = new unsigned char[hashLenght];
			memcpy(tmp, hashes_[i].hash, hashLenght);
			memcpy(hashes_[i].hash, hashes_[k].hash, hashLenght);
			memcpy(hashes_[k].hash, tmp, hashLenght);
			delete[] tmp;
			int temp = hashes_[i].id;
			hashes_[i].id = hashes_[k].id;
			hashes_[k].id = temp;
		}
	}

	HashNode* buildHashTree(HashNode* root, HashNode elem) {
		if (root == NULL) {
			root = new HashNode;
			root->hash = elem.hash;
			root->id = elem.id;
			root->left = root->right = NULL;
		}
		else if (root->id > elem.id)
			root->left = buildHashTree(root->left, elem);
		else
			root->right= buildHashTree(root->right, elem);
		return root;
	}

	void getTmp5(HashNode* root, unsigned char* temp1, int count, int ind, int iter) {
		/*if (iter == 0) {
			memcpy(temp1, root->hash, count);
		}*/
		if (root->id == ind) {
			for (int j = 0; j < count; j++) {
				temp1[j] ^= (root->hash[j] << iter);
			}
			return;
		}
		else if (root->id > ind) {
			for (int j = 0; j < count; j++) {
				temp1[j] ^= (root->hash[j] << iter);
			}
			getTmp5(root->left, temp1, count, ind, iter++);
		}
		else {
			for (int j = 0; j < count; j++) {
				temp1[j] ^= (root->hash[j] << iter);
			}
			getTmp5(root->right, temp1, count, ind, iter++);
		}
		return;
	}

	unsigned char* findHash(HashNode* root, int ind) {
		if (root->id == ind) {
			return root->hash;
		}
		else if (root->id > ind) {
			findHash(root->left, ind);
		}
		else {
			findHash(root->right, ind);
		}
	}

	void setHash(HashNode* root, int ind, unsigned char* hash, int lenght) {
		if (root->id == ind) {
			memcpy(root->hash, hash, lenght);
		}
		else if (root->id > ind) {
			findHash(root->left, ind);
		}
		else {
			findHash(root->right, ind);
		}
	}

	void freemem(HashNode* root) {
		if (root != NULL) {
			freemem(root->left);
			freemem(root->right);
//			delete[] root->hash;
			delete[] root;
		}
	}

	void pbkdf2(hmac& hmac, const unsigned char* salt, size_t salt_len, int iterations, unsigned char* dk, size_t dklen)
	{
		size_t hlen = hmac.hashsize() / 8;
		unsigned char* res = dk;
		unsigned char* temp1 = new unsigned char[hlen * 2];
		//unsigned char** hashes = new unsigned char*[iterations];
		//for (int count = 0; count < iterations; count++)
		//	hashes[count] = new unsigned char[hlen * 2];
		size_t remaining = dklen;

		HashNode* hashes_ = new HashNode[iterations];
		for (int count = 0; count < iterations; count++) {
			hashes_[count].id = count;
			hashes_[count].hash = new unsigned char[hlen * 2];
		}

		HashNode* root = NULL;

		//hashesInit(hashes, salt, iterations, hlen * 2, salt_len, 12345);
		hashesNodeInit(hashes_, salt, iterations, hlen * 2, salt_len, 12345);
		for (int count = 0; count < iterations; count++) {
			root = buildHashTree(root, hashes_[count]);
		}


		for (uint32_t i = 0; res < dk + remaining; i++)
		{
			hmac.init();
			hmac.update(salt, salt_len);
			uint32_t ir = swap_uint32(i + 1);
			hmac.update((const unsigned char*)&ir, sizeof(ir));
			hmac.final(temp1);
			size_t sz = min(hlen, remaining);
			memcpy(res, temp1, sz);
			int x = 0;
			int y = 0;
			for (int c = 1; c < iterations; c++)
			{
				x = (x + 1) % (iterations - 1);
				//y = (y + hashes[x][c % (hlen * 2)]) % (iterations - 1);
				//unsigned char* tmp = new unsigned char[hlen * 2];
				//memcpy(tmp, hashes[x], hlen * 2);
				//memcpy(hashes[x], hashes[y], hlen * 2);
				//memcpy(hashes[y], tmp, hlen * 2);
				//delete[] tmp;
				//int t = (hashes[x][c % (hlen * 2)] + hashes[y][c % (hlen * 2)]) % (iterations - 1);
				//getTmp4(hashes, temp1, iterations, hlen * 2);
				//memcpy(hashes[t], temp1, hlen * 2);
				y = (y + x + c) % (iterations - 1);
				unsigned char* tmp = new unsigned char[hlen * 2];
				memcpy(tmp, findHash(root, x), hlen * 2);
				setHash(root, x, findHash(root, y), hlen * 2);
				setHash(root, y, tmp, hlen * 2);
				delete[] tmp;
				int t = (x + y) % (iterations - 1);
				getTmp5(root, temp1, hlen * 2, t, 0);
				hmac.hash_string(temp1, hlen, temp1 + hlen);
				for (size_t i = 0; i < sz; i++)
				res[i] ^= temp1[hlen + i];
				memcpy(temp1, temp1 + hlen, hlen);
			}
			res += sz;
		}
		
		delete[] temp1;
		freemem(root);
		
		for (int count = 0; count < iterations; count++)
			delete[] hashes_[count].hash;
		delete[] hashes_;
		//for (int count = 0; count < iterations; count++)
		//	delete[] hashes[count];
		//delete[] hashes;
	}

	
}